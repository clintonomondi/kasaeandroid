import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kasaeunited/pages/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'api.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
  // TODO: Add text editing controllers (101)

}

class _LoginState extends State<Login> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var email;
  var password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('LOGIN'),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.grey[200],
          child: ListView(
            padding: EdgeInsets.only(top: 10.0, left: 24.0, right: 24.0),
            children: <Widget>[
              SizedBox(
                height: 155.0,
                child: Image.asset(
                  "assets/logo.jpeg",
                  fit: BoxFit.contain,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(0.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 40.0),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: 'Phone',
                            prefixIcon: Icon(Icons.phone),
                          ),
                          validator: (emailValue) {
                            if (emailValue.isEmpty) {
                              return 'Please enter phone number';
                            }
                            email = emailValue;
                            return null;
                          },
                        ),
                        SizedBox(height: 12.0),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autofocus: false,
                          obscureText: true,
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.vpn_key),
                            hintText: 'Password',
                          ),
                          validator: (passwordValue) {
                            if (passwordValue.isEmpty) {
                              return 'Please enter password';
                            }
                            password = passwordValue;
                            return null;
                          },
                        ),
                     
                      ]),
                ),
                
              ),
                 SizedBox(height: 12.0),
                        SizedBox(height: 12.0),
                        FlatButton(
                          child: Text(_isLoading ? 'Please wait...' : 'Signin',
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                              )),
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          // shape: new RoundedRectangleBorder(
                          //     borderRadius: new BorderRadius.circular(20.0)),
                          color: Colors.lightBlue[900],
                          disabledColor: Colors.grey,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _login();
                            }
                          },
                        ),
            ],
          ),
        ),
      ),
    );
  }

  void _login() async {
    try{
    setState(() {
      _isLoading = true;
    });
    var data = {'email': email, 'password': password};
    var res = await Network().authData(data, 'login');
    var body = json.decode(res.body);
    if (body['status']) {
       _toastsuccess(body['Successful']);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['access_token']);
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => HomePage()),
      );
    } else {
      _toasterror(body['message']);
    }
    }catch(e){
      _toasterror('Server connection error');
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _toasterror(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red[800],
        textColor: Colors.white,
        fontSize: 16.0);
  }
  void _toastsuccess(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.green[800],
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
