import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kasaeunited/auth/Login.dart';
import 'package:kasaeunited/auth/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'addmember.dart';

class Members extends StatefulWidget {
  @override
  _MembersState createState() => _MembersState();
}

class _MembersState extends State<Members> {
  var members;
  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Members'),
      ),
      body: new ListView.builder(
        padding: const EdgeInsets.all(6),
        itemCount: members == null ? 0 : members.length,
        itemBuilder: (context, i) {
          return Container(
            height: 130,
            child: FlatButton(
              onPressed: () {},
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                        width: 100.0,
                        height: 100.0,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          image: DecorationImage(
                              image: new NetworkImage(
                                  Network().imageurl + members[i]['pic']),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.all(Radius.circular(75.0)),
                          boxShadow: [
                            new BoxShadow(
                                blurRadius: 5.0, offset: new Offset(2.0, 5.0))
                          ],
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: new Container(
                      margin: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
                      child: new Column(
                        children: [
                          new Text(
                            members[i]['name'],
                            style: new TextStyle(
                                fontSize: 12.0,
                                fontFamily: 'Arvo',
                                fontWeight: FontWeight.bold,
                                color: Colors.lightBlue[900]),
                          ),
                          new Padding(padding: const EdgeInsets.all(2.0)),
                          new Text(
                            members[i]['role'],
                            maxLines: 3,
                            style: new TextStyle(
                                color: const Color(0xff8785A4),
                                fontFamily: 'Arvo'),
                          ),
                          new Text(
                            members[i]['email'],
                            maxLines: 3,
                            style: new TextStyle(
                                color: const Color(0xff8785A4),
                                fontFamily: 'Arvo'),
                          ),
                          new Text(
                            "" + members[i]['amount'].toString(),
                            maxLines: 3,
                            style: new TextStyle(
                                color: const Color(0xff8785A4),
                                fontFamily: 'Arvo'),
                          ),
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
        floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Colors.lightBlue[900]),
        ),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddMembers()),
            );
          },
          child: Icon(Icons.person_add),
        ),
      ),
    );
  }

  void getUsers() async {
    try {
      var res = await Network().getData('getMembers');
      var body = json.decode(res.body);
      if (body['message'] == 'Unauthenticated.') {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.remove('token');
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Login()));
        _toasterror("Please login!");
      }
      setState(() {
        members = body['data'];
      });
    } catch (e) {}
  }

  void _toasterror(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red[800],
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
