import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kasaeunited/auth/api.dart';
import 'package:kasaeunited/pages/addmember.dart';
import 'package:kasaeunited/pages/members.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../auth/Login.dart';
import 'package:pie_chart/pie_chart.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var email = "";
  var name = "";
  var pic = "http://i.pravatar.cc/300";
  var issues = "0";
  var members = "0";
  var saving = "0.0";
  double male = 0.0;
  double female = 0.0;
  double other = 0.0;
  bool toggle = false;

  List<Color> colorList = [
    Colors.red,
    Colors.blue[900],
    Colors.green,
  ];

  @override
  void initState() {
    super.initState();
    getUser();
    getSummery();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              print('Click search');
            },
          ),
          IconButton(
            icon: Icon(Icons.star),
            onPressed: () {
              print('Click start');
            },
          ),
        ],
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text(name),
              accountEmail: new Text(email),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: new NetworkImage(pic),
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.home),
                title: new Text('Home'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.people),
                title: new Text('Members'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Members()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.attach_money),
                title: new Text('Monthly'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.money_off),
                title: new Text('Trans'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.signal_wifi_off),
                title: new Text('Single'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.book),
                title: new Text('Roles'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.email),
                title: new Text('Contact'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.person),
                title: new Text('Account'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );
                },
              ),
            ),
            FlatButton(
              child: ListTile(
                leading: Icon(Icons.vpn_key),
                title: new Text('Logout'),
                onTap: () {
                  Navigator.pop(context);
                  logout();
                },
              ),
            ),
          ],
        ),
      ),
      body: Center(
          child: Container(
        color: Colors.grey[200],
        child: ListView(
          padding: EdgeInsets.only(top: 10.0, left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 40.0),
            Text(
              "WELCOME " + name,
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
            ),
            SizedBox(height: 40.0),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  onPressed: () => {},
                  color: Colors.white,
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(
                        Icons.people,
                        color: Colors.cyan[600],
                      ),
                      Text(members + " Members")
                    ],
                  ),
                ),
                RaisedButton(
                  onPressed: () => {},
                  color: Colors.white,
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(
                        Icons.attach_money,
                        color: Colors.lightBlue[900],
                      ),
                      Text(saving + "  Amount")
                    ],
                  ),
                ),
                RaisedButton(
                  onPressed: () => {},
                  color: Colors.white,
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(
                        Icons.settings,
                        color: Colors.cyan[600],
                      ),
                      Text(issues + "  Issues")
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Divider(
              color: Colors.black,
            ),
            SizedBox(height: 10.0),
            map()
          ],
        ),
      )),
      floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Colors.lightBlue[900]),
        ),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddMembers()),
            );
          },
          child: Icon(Icons.person_add),
        ),
      ),
    );
  }

  void logout() async {
    try {
      var res = await Network().getData('auth/logout');
      var body = json.decode(res.body);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.remove('token');
      Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
    } catch (e) {}
  }

  void getUser() async {
    try {
      var res = await Network().getData('user');
      var body = json.decode(res.body);
      if (body['message'] == 'Unauthenticated.') {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.remove('token');
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Login()));
        _toasterror("Please login!");
      }
      setState(() {
        name = body['data']['name'];
        email = body['data']['email'];
        if (body['data']['pic'] != null) {
          pic = Network().imageurl + body['data']['pic'];
        }
      });
    } catch (e) {}
  }

  void getSummery() async {
    var res = await Network().getData('getSummery');
    var body = json.decode(res.body);
    if (body['message'] == 'Unauthenticated.') {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.remove('token');
      Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
      _toasterror("Please login!");
    }
    setState(() {
      issues = body['data']['issues'].toString();
      members = body['data']['members'].toString();
      saving = body['data']['saving']['amount'].toString();
      male = double.parse(body['gender'][0].toString());
      female = double.parse(body['gender'][1].toString());
      other = double.parse(body['gender'][2].toString());
    });
  }

  void _toasterror(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red[800],
        textColor: Colors.white,
        fontSize: 16.0);
  }

  map() {
    Map<String, double> dataMap = Map();
    dataMap.putIfAbsent("Male", () => male);
    dataMap.putIfAbsent("Female", () => female);
    dataMap.putIfAbsent("Others", () => other);
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: 800),
      chartLegendSpacing: 32.0,
      chartRadius: MediaQuery.of(context).size.width / 2.7,
      showChartValuesInPercentage: true,
      showChartValues: true,
      showChartValuesOutside: false,
      chartValueBackgroundColor: Colors.grey[200],
      colorList: colorList,
      showLegends: true,
      legendPosition: LegendPosition.right,
      decimalPlaces: 1,
      showChartValueLabel: true,
      initialAngle: 0,
      chartValueStyle: defaultChartValueStyle.copyWith(
        color: Colors.blueGrey[900].withOpacity(0.9),
      ),
      chartType: ChartType.disc,
    );
  }
}

// TODO: Add AccentColorOverride (103)
