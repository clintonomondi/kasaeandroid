import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kasaeunited/auth/api.dart';
import 'package:kasaeunited/pages/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddMembers extends StatefulWidget {
  @override
  _AddMembersState createState() => _AddMembersState();
}

class _AddMembersState extends State<AddMembers> {
bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var email;
  var password;

@override
  void initState() {
     super.initState();
    // getUsers();
   
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add New Members'),
      ),
        body: SafeArea(
        child: Container(
          color: Colors.grey[200],
          child: ListView(
            padding: EdgeInsets.only(top: 10.0, left: 24.0, right: 24.0),
            children: <Widget>[
            
              Padding(
                padding: const EdgeInsets.all(0.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 40.0),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: 'Full Name',
                            prefixIcon: Icon(Icons.person),
                          ),
                          validator: (emailValue) {
                            if (emailValue.isEmpty) {
                              return 'Please enter name';
                            }
                            email = emailValue;
                            return null;
                          },
                        ),
                        SizedBox(height: 12.0),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: 'Gender',
                            prefixIcon: Icon(Icons.home),
                          ),
                          validator: (emailValue) {
                            if (emailValue.isEmpty) {
                              return 'Please enter gender';
                            }
                            email = emailValue;
                            return null;
                          },
                        ),
                        SizedBox(height: 12.0),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: 'Phone Number',
                            prefixIcon: Icon(Icons.phone),
                          ),
                          validator: (emailValue) {
                            if (emailValue.isEmpty) {
                              return 'Please enter phone number';
                            }
                            email = emailValue;
                            return null;
                          },
                        ),
                        SizedBox(height: 12.0),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: 'ID Number',
                            prefixIcon: Icon(Icons.book),
                          ),
                          validator: (emailValue) {
                            if (emailValue.isEmpty) {
                              return 'Please enter ID number';
                            }
                            email = emailValue;
                            return null;
                          },
                        ),
                     
                      ]),
                ),
                
              ),
                 SizedBox(height: 12.0),
                        SizedBox(height: 12.0),
                        FlatButton(
                          child: Text(_isLoading ? 'Please wait...' : 'Register',
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                              )),
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          // shape: new RoundedRectangleBorder(
                          //     borderRadius: new BorderRadius.circular(20.0)),
                          color: Colors.lightBlue[900],
                          disabledColor: Colors.grey,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              // _login();
                            }
                          },
                        ),
            ],
          ),
        ),
      ),
        floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Colors.lightBlue[900]),
        ),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
          child: Icon(Icons.home),
        ),
      ),
    );
  }

  // void getUsers() async {
  //   var res = await Network().getData('getMembers');
  //   var body = json.decode(res.body);
  //   setState(() {
  //     members = body['data'];
  //   });
  //   print(members);
  // }
}